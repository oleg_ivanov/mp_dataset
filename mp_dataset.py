import multiprocessing as mp
import numpy as np
import time
import copy

DEBUG = 0

###################################################################################################
# working logic:                                                                                  # 
# pool_indexes -- indexes of batch to be processed. If it >-1, we process it, otherwise we skip it#
# pool_flags -- our inner flags, to show that batch is ready for parent class to obtain           #
# pool_flags>0 --> process is busy, ==0 --> ready to work/results are ready                       #
#### WE ASSUME HERE THAT ALL INTERPROCESS COMMUNICATIONs ARE PERFORMED VIA mp.Array and pipes  ####
###################################################################################################
class dataset_worker:
    def __init__(self,idata,pool,pool_range,pool_flags1,pool_flags2,pool_indexes,n,worker_wsize):
        self.dataset = idata
        self.idx = np.arange(0,1000)
        self.ID = n
        self.wsize = worker_wsize

        self.pool = pool
        self.pool_range = pool_range
        self.pool_flags1 = pool_flags1
        self.pool_flags2 = pool_flags2

        self.pool_indexes = pool_indexes 

    def process_batches(self,pipe_worker_batch):
        while 1==1:
            if self.pool_indexes[self.pool_range*self.wsize]>-1 and self.pool_flags1[self.pool_range]!=self.pool_indexes[self.pool_range*self.wsize] and self.pool_flags2[self.pool_range]==0:
                self.pool_flags1[self.pool_range] = -2
                if DEBUG:
                    print("batch changing status!! for :",self.pool_range,"got indexes:",self.pool_indexes[self.pool_range*self.wsize:self.pool_range*self.wsize+self.wsize])

                #d2 = []
                for i in  range(self.pool_range*self.wsize,self.pool_range*self.wsize+self.wsize):
                    data = self.dataset.get_batch([self.pool_indexes[i]])
                    data = data.astype(np.float32)
                    #d2+=[data]
                    self.pool[i*self.dataset.batch_size():(i+1)*self.dataset.batch_size()]=data.reshape(-1)[:]

                #s1 = np.mean(np.abs(np.concatenate(d2,axis=0).reshape(len(d2),-1)),axis=-1)
                #s2 = self.pool_indexes[self.pool_range*self.wsize:self.pool_range*self.wsize+self.wsize]
                #if np.mean(np.abs(s1-s2))>0:
                #    print("ACHTUNG!!!BADWORKER:",s1,s2)
                ### sending signal for all worker
                pipe_worker_batch.send([self.pool_range,self.pool_indexes[self.pool_range*self.wsize]])
                self.pool_flags1[self.pool_range]=self.pool_indexes[self.pool_range*self.wsize]#ready
                if DEBUG:
                    print("worker ",self.pool_range," is ready")
            time.sleep(0.0001)

class dataset_mp:
    def __init__(self,batch_size,workers = 4,worker_wsize = 16,idata = None):
        """ batch_size(num of elements) for 1 batch, workers -- number of processes to fork,
            worker_wsize -- max number of batches to be prepared by worker """

        self.dataset_raw = idata

        self.batch_pool_unused= []
        self.batch_pool_unused_idx = []

        
        self.workers = workers
        self.worker_wsize = worker_wsize
        self.batch_pool = mp.RawArray("f",range(workers*worker_wsize*self.dataset_raw.batch_size()))

        self.batch_pool_np = np.frombuffer(self.batch_pool, dtype=np.float32)

        
        self.pool_indexes = mp.RawArray("i",range(workers*worker_wsize))
        self.status = mp.RawArray("i",1)


        self.pool_flags1 = mp.RawArray("i",range(workers))
        self.pool_flags2 = mp.RawArray("i",range(workers))

        for i in range(len(self.pool_flags1)):
            self.pool_flags1[i]=0
            self.pool_flags2[i]=0

        for i in range(len(self.pool_indexes)):
            self.pool_indexes[i]=-1


        self.pipe_worker_batch = [mp.Pipe(duplex=False) for f in range(workers)]
        self.pipe_batch_idxgen = [mp.Pipe(duplex=False) for f in range(workers)]




        self.datasets = [dataset_worker(self.dataset_raw,self.batch_pool,f,self.pool_flags1,self.pool_flags2,self.pool_indexes,f,self.worker_wsize) for f in range(workers+1)]
        self.wpool_work = []
        for i in range(workers):
            func = self.datasets[i].process_batches
            self.wpool_work+=[mp.Process(target=func,args=(self.pipe_worker_batch[i][1],))] 
            self.wpool_work[-1].start()


        from idx_gen1d import idx_gen1d

        self.state = "train_uniform"
        self.train = 0
        self.uniform = 0

        self.idx_generator = self.dataset_raw.idx_generator_train





    def set_state(self,state):
        if DEBUG:
            print("SENDING STATE!!!")
        self.state_pipe[1].send(state)
        self.batch_pool_unused = []
        self.batch_pool_unused_idx = []

        flag = 1

        while flag ==1:
            flag = 0
            for i in range(len(self.pool_flags1)):
                if self.pipe_worker_batch[i][0].poll(0.001)==True:
                    tmp = self.pipe_worker_batch[i][0].recv()
                    flag = 1
                    break

        
        time.sleep(0.01)
        for i in range(len(self.pipe_batch_idxgen)):
            self.pipe_batch_idxgen[i][1].send(1)

        time.sleep(0.1)


    def set_state_(self,state):
        if DEBUG:
            print("stopping threads.....")
        self.status = 0
        self.state = state
        self.train = 0

        if self.state.find("train")>-1:
            self.train = 0

        if self.state.find("test")>-1:
            self.train = 1

        if self.state.find("valid")>-1:
            self.train = 2


        self.uniform = 0

        if self.state.find("hist")>-1:
            self.uniform = 1

        if self.state.find("seq")>-1:
            self.uniform = 2

        if self.state.find("seq")>-1 and self.state.find("random")>-1:
            self.uniform = 3



        if DEBUG:
            print("STATE:",self.train,self.uniform)


    def get_idx(self,bsize):
        if DEBUG:
            print('get idx, params:',self.train,self.uniform)
        if self.train == 0:
            if self.uniform == 0:#pure random idx gen
                return self.dataset_raw.idx_generator_train.get_uniform(bsize)
            if self.uniform == 1:#histogrammed idx gen
                return self.dataset_raw.idx_generator_train.get_histogrammed(bsize)
            if self.uniform == 2:#sequential idx gen
                return self.dataset_raw.idx_generator_train.get_seq(bsize)
            if self.uniform == 3:#sequential idx gen with random permutation
                return self.dataset_raw.idx_generator_train.get_seq(bsize)

        if self.train == 1:
            if self.uniform == 0:#pure random idx gen
                return self.dataset_raw.idx_generator_test.get_uniform(bsize)
            if self.uniform == 1:#histogrammed idx gen
                return self.dataset_raw.idx_generator_test.get_histogrammed(bsize)
            if self.uniform == 2:#sequential idx gen
                return self.dataset_raw.idx_generator_test.get_seq(bsize)
            if self.uniform == 3:#sequential idx gen with random permutation
                return self.dataset_raw.idx_generator_test.get_seq(bsize)

        if self.train == 2:
            if self.uniform == 0:#pure random idx gen
                return self.dataset_raw.idx_generator_valid.get_uniform(bsize)
            if self.uniform == 1:#histogrammed idx gen
                return self.dataset_raw.idx_generator_valid.get_histogrammed(bsize)
            if self.uniform == 2:#sequential idx gen
                return self.dataset_raw.idx_generator_valid.get_seq(bsize)
            if self.uniform == 3:#sequential idx gen with random permutation
                return self.dataset_raw.idx_generator_valid.get_seq(bsize)



    def start_batches_(self,pipe_state,pipe_batch):
        while  1==1:
            for i in range(len(self.wpool_work)):
                if pipe_batch[i][0].poll(0.001)==True and self.pool_flags1[i]!=-2:
                    if DEBUG:
                        print("batch ",i," is ready, setting worker")
                    state = pipe_batch[i][0].recv()
                    if DEBUG:
                        print("got state:",state)
                    if state<0:
                        continue
                    idx = self.get_idx(self.worker_wsize)
                    if DEBUG:
                        print("setting idx for worker:",i,"to",idx)
                    for j in range(len(idx)):
                        self.pool_indexes[i*self.worker_wsize+j]=idx[j]
            if pipe_state.poll(0.01) == True:
                if DEBUG:
                    print("got change state event")
                for i in range(len(self.pool_indexes)):
                    self.pool_indexes[i] = -1
                state = pipe_state.recv()
                self.set_state_(state)

    def start_batches(self):
        if DEBUG:
            print("starting threads......")
        self.last_idx = []
        self.status = 1 

 

        self.state_pipe = mp.Pipe(duplex=False)
        self.pr = mp.Process(target=self.start_batches_,args=(self.state_pipe[0],self.pipe_batch_idxgen))
        self.pr.start()


  
    def get_batch_uniform(self,batch_size):
        import itertools
        rez = []
        rez_idx = []

        b_counter = 0
        if DEBUG:
            print("unused batch pool:",self.batch_pool_unused_idx)
        if len(self.batch_pool_unused)>0:
            maxidx = min(self.batch_pool_unused.shape[0],batch_size)
            rez=[self.batch_pool_unused[:maxidx]]
            rez_idx=[self.batch_pool_unused_idx[:maxidx]]

            b_counter+=maxidx

            self.batch_pool_unused=self.batch_pool_unused[maxidx:]
            self.batch_pool_unused_idx=self.batch_pool_unused_idx[maxidx:]

            if rez[0].shape[0]>=batch_size:
                return rez[0],rez_idx[0]
                        

        
        ready_signals = []
        while b_counter<batch_size:
            for i in range(self.workers):
                if self.pipe_worker_batch[i][0].poll(0.001) == True:
                    w_idx,idx = self.pipe_worker_batch[i][0].recv()
                    if DEBUG:
                        print("===GET BATCH got signals from batch:",i,"sig:",w_idx,idx)
                    if idx<0:
                        continue
                    self.pool_flags2[i]=1
                    tmp = self.batch_pool_np[(w_idx)*self.dataset_raw.batch_size()*self.worker_wsize:(w_idx)*self.dataset_raw.batch_size()*self.worker_wsize+self.dataset_raw.batch_size()*self.worker_wsize]
                    sh = list(copy.deepcopy(self.dataset_raw.batch_shape))
                    sh[0]*=self.worker_wsize
                    tmp = tmp.reshape(*sh)
                    rez+=[tmp]
                    rez_idx+=[self.pool_indexes[w_idx*self.worker_wsize:(w_idx+1)*self.worker_wsize]]

                    s1 = np.mean(tmp.reshape(tmp.shape[0],-1),axis=-1)
                    s2 = np.array(rez_idx[-1])

                    if DEBUG:
                        print("s1,s2:",s1,s2)
                        diff = np.sum(np.abs(s1-s2))
                        if diff>0:
                            print("###ACHTUNG!!! values:",s1,s2)

                    
                    b_counter+=tmp.shape[0]

                    ready_signals+=[[i,idx]]
                    #print("===sending ready signal for:",i,"idx=",idx)
                    #self.pipe_batch_idxgen[i][1].send(idx)


                    if b_counter>=batch_size:
                        break



            time.sleep(0.001)


        rez = np.concatenate(rez,axis=0)
        rez_idx = list(np.array(rez_idx).flat)

        if rez.shape[0]>batch_size:
            if len(self.batch_pool_unused) ==0:
                self.batch_pool_unused = rez[batch_size:]
                self.batch_pool_unused_idx = rez_idx[batch_size:]
            else:
                self.batch_pool_unused = np.concatenate([self.batch_pool_unused,rez[batch_size:]],axis=0)
                self.batch_pool_unused_idx +=rez_idx[batch_size:]

        rez = rez[:batch_size]
        rez_idx = rez_idx[:batch_size]
               
        for i in range(len(ready_signals)):
            if DEBUG:
                print("===sending ready signal for:",ready_signals[i][0],"idx=",ready_signals[i][1])
            self.pool_flags2[ready_signals[i][0]]=0
            self.pipe_batch_idxgen[ready_signals[i][0]][1].send(ready_signals[i][1])
  
        return rez,rez_idx
