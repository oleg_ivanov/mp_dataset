import numpy as np
import copy
class idx_gen1d:
    def __init__(self,idx,values):
        self.idx = idx
        self.values = values
        self.iterator = None

    def split1(self,bins):
        self.indexed_bins = [[] for f in range(len(bins))]
        self.bins = copy.deepcopy(bins)
        for i in range(len(self.idx)):
            pos = int(np.argmin(np.abs(bins-self.values[i])))
            self.indexed_bins[pos]+=[self.idx[i]]

        #print("Total objects:",len(self.idx))
        #print("bins filling:",[len(self.indexed_bins[i]) for i in  range(len(self.indexed_bins))])

    def init_uniform(self,bins):
        self.split1(bins)
            
    def get_uniform(self,bsize):
        rez = []
        for i in range(bsize):
            n = int(np.random.uniform(0,len(self.bins)))
            while len(self.indexed_bins[n])==0:
                n = int(np.random.uniform(0,len(self.bins)))
            n2 = int(np.random.uniform(0,len(self.indexed_bins[n])))
            rez+=[self.indexed_bins[n][n2]]
        return rez

    def init_hist(self,hist,bins):
        centers = np.diff(bins)/2.
        pts = bins[:-1]+centers 
        print("pts:",pts)
        s = np.cumsum(hist)
        print("s:",s)
        s-= s[0]
        s/= s[-1]
        self.cdf_pts = pts 
        self.cdf = s
        self.split1(bins)
        self.centers=centers

    def get_histogrammed(self,bsize):
        idx = np.interp(np.random.random(bsize),self.cdf,self.cdf_pts)
        idx2=[]
        for i in range(len(idx)):
            idx2+=[np.argmin(np.abs(self.bins-idx[i]))]
        idx = np.array(idx2)
        
        idx=idx.astype(int)
        idx[idx>len(self.bins)-1] = len(self.bins)-1
        idx[idx<0]=0
        rez = []
        for i in range(bsize):
            n2 = int(np.random.uniform(0,len(self.indexed_bins[idx[i]])))
            rez+=[self.indexed_bins[idx[i]][n2]]
        return np.array(rez)
        
    def get_seq(self,bsize):
        if self.iterator is None:
            self.iterator = iter(zip(self.idx,np.arange(len(self.idx))))
        rez = []
        for i in range(bsize):
            try:
                idx, c = next(self.iterator)
            except:
                self.iterator = None
                self.iterator = iter(zip(self.idx,np.arange(len(self.idx))))

                return rez+[-1]

            rez+=[idx]
        return rez
 
    def get_seq_randomized(self,bsize):
        if self.iterator is None:
            self.iterator = iter(zip(np.random.permutation(self.idx),np.arange(len(self.idx))))
        rez = []
        for i in range(bsize):
            try:
                idx, c = next(self.iterator)
            except:
                self.iterator = None
                self.iterator = iter(zip(np.random.permutation(self.idx),np.arange(len(self.idx))))
                return rez+[-1]
            rez+=[idx]
        return rez
