# mp_dataset is primitive but efficient library for distributed mini-batch generation for neural networks training
## Motivation
Standard NN training pipeline includes three main steps: batch generation, calculation of model and its gradients vs batch, and model weight optimization.
For fast trainig we need to consume as much GPU in a period of time as possible. However, if the training requires strong batch preprocessing,
which in 99% cases is performed on CPU, GPU will rest signifcant amount of time, waiting for new batches.

The first obvious solution is to perform multi-threaded batch preprocessing. If the threads are created every time we need distributed processing,
as is the case in OMP-powered libraries such as NumPy,  we spend a lot of time for threads creation and synchronisation.
For the scheme with asynchronously working threads we also meet the problem of thread synchronization (Python GIL) which will strongly decrease the overall 
performance.

To avoid GIL one should use multi-process batch preparation, and to increase performance, one should use asynchronous batch preparation scheme.
In this simple library calculations are performed with forked batch preprocessing routines, which are synchronized via mp.RawArray for batch data
and via Unix pipes for processing synchronization.

In table below shown the performance for the  toy example included in repo (training of image classification on GPU) -- main.py

| Mode                   | Workers | Examples/worker | Iterations/sec | GPU consuming |
|:----------------------:|:-------:|-----------------|:--------------:|:-------------:|
| GPU only, no batch gen | 0       | 10              | ~14.5          | 99%           |
| Standard batch gen     | 1       | 10              | ~2.6           | 20-40%        |
| Distributed batch gen  | 10      | 10              | ~10-12         | >90%          |

The tests we performed on PC with 20-core (in hyper threading) Intel i9 and NVIDIA Geforce GTX 1080. GPU consuming
was measured with nvidia-smi -l.


We see that batch generation with 1 thread consumes ~ 10 iterations/sec -- all this time GPU is resting!

## Usage

mp_dataset assumes that we already have standard dataset-reading class with implemented functions (see dummy_dataset in main.py): 

  * get_batch(batch_size) -- get batch size in numpy array
  * batch_size() -- expected size of 1 batch element
  * all_idx -- numpy array of all id numbers for examples from dataset
  * train_idx -- numpy array for train example ids
  * test_idx -- numpy array for test example ids
  * valid_idx -- numpy array for validation example ids
  * idx_generator_train -- idx_gen1d class for train ids
  * idx_generator_test -- idx_gen1d class for test ids
  * idx_generator_valid -- idx_gen1d class for valid ids

idx_gen1d is a simple id number generator, capable of generating ids in modes:
  * sequential (standard, for testing)
  * sequential with random permutation (standard, for training)
  * random id generation from uniform distribution
  * random id generation from given by histogram distribution (each id is accompanied with some real value) 


At init mp_dataset should be provided with already initialized dataset instance.
After that start_batches() should be called to start generation threads.

Ids generation mode is set via set_state(mode). Mode is variable of type string, mode changing
is performed via regexp matching on keys "train", "test" and "valid" for id set and keys "hist", "seq", "random" for id generation.


For example: mode="train seq" will generate ids from train set sequentially, without random permutation.
mode = "train random seq" will generate ids from train set sequentially, but initial set will be random permutated.