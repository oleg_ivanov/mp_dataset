import numpy as np
import time
import cv2
import torch
import torch.nn as nn
from tqdm import tqdm

from mp_dataset import dataset_mp

class Model1(nn.Module):
    def __init__(self):
        super(Model1,self).__init__()
        self.conv1 = nn.Sequential(nn.Conv2d(3,16,3),nn.ReLU(),nn.MaxPool2d(2,2),nn.Conv2d(16,64,3),nn.ReLU(),nn.MaxPool2d(2,2),nn.Conv2d(64,128,3),nn.ReLU(),nn.MaxPool2d(2,2),nn.Conv2d(128,512,3),nn.ReLU(),nn.MaxPool2d(2,2))
        self.conv2d = nn.Sequential(nn.Conv2d(512,512,1),nn.ReLU(),nn.Conv2d(512,16,3))
        self.smax = nn.Softmax2d()

    def forward(self,inp):
        inp1 = self.conv1(inp)
        inp2 = self.conv2d(inp1)
        return self.smax(inp2)
        



class dummy_dataset:
    def __init__(self,wx,wy):
        from idx_gen1d import idx_gen1d

        self.wx = wx
        self.wy = wy

        self.train_idx = np.arange(0,10000)
        self.test_idx = np.arange(10000,13000)
        self.valid_idx = np.arange(13000,16000)


        self.idx_generator_train = idx_gen1d(self.train_idx,self.train_idx*0+1)
        self.idx_generator_test = idx_gen1d(self.test_idx,self.test_idx*0+1)
        self.idx_generator_valid = idx_gen1d(self.valid_idx,self.valid_idx*0+1)

        self.batch_bytes = 0

        self.all_idx = np.arange(0,10000)
        self.train_idx = np.arange(0,7000)
        self.test_idx = np.arange(7000,9000)
        self.valid_idx = np.arange(9000,10000)

        self.idx_generator_train = idx_gen1d(self.train_idx,self.train_idx*0+1)
        self.idx_generator_train.init_uniform(np.array([0,1]))

        self.idx_generator_test = idx_gen1d(self.test_idx,self.test_idx*0+1)
        self.idx_generator_test.init_uniform(np.array([0,1]))

        self.idx_generator_valid = idx_gen1d(self.valid_idx,self.valid_idx*0+1)
        self.idx_generator_valid.init_uniform(np.array([0,1]))



    def gen_img(self,num):
        rez = np.zeros([self.wy,self.wx,3],dtype=np.uint8) 
        cv2.putText(rez,"Example:"+str(num), (rez.shape[1]//3,rez.shape[0]//2), 2, 2, (255,255,255),2)
        return rez

    def random_affine(self,img):
        theta = np.random.uniform(-90,90)
        scale = np.random.uniform(0.8,1.1)
        transform = cv2.getRotationMatrix2D((img.shape[1]//2,img.shape[0]//2),theta,scale)
        rez = cv2.warpAffine(img,transform,(img.shape[1],img.shape[0]))
        return rez


    def process_img(self,img):
        for i in range(0,30):#int(np.random.uniform(100,500))):
            img = self.random_affine(img)
        return img

    def get_batch(self,nums):
        rez = []
        for i in range(len(nums)):
            rez+=[np.expand_dims(self.process_img(self.gen_img(nums[i])),axis=0)]
            rez[-1]=rez[-1]*0+nums[i]
        return np.concatenate(rez,axis=0)

    def batch_size(self):
        if self.batch_bytes == 0 :
            tmp = self.get_batch([0])
            tmp_s = 1
            for i in tmp.shape:
                tmp_s *= i
            self.batch_bytes = tmp_s 

            self.batch_shape = tmp.shape

        return self.batch_bytes

    def batch_shape(self):
        return self.batch_shape



      


def learn_sequential(device,skip=False):
    model = Model1().to(device)
    raw_data = dummy_dataset(512,384)
    optimizer = torch.optim.Adam(model.parameters(),lr=1e-3)

    batch_size = 10 
    to_gen = 300
    for epoch in range(1):
        print("### epoch:",epoch)
        start = time.perf_counter()
        inp = []
        for it in tqdm(range(to_gen),ascii=True):
            if it==0 and skip==True:
                inp  = raw_data.get_batch((np.random.rand(batch_size)))
                inp = np.transpose(inp,axes=(0,-1,1,2))
            elif skip==False:
                inp  = raw_data.get_batch((np.random.rand(batch_size)))
                inp = np.transpose(inp,axes=(0,-1,1,2))
            out = model(torch.tensor(inp).float().to(device))
            target = torch.rand(out.size()).to(device)
            loss = torch.mean(torch.pow(out-target,2))
            loss.backward()
        print("epoch processing time:",time.perf_counter()-start)
   

def learn_distributed(device):
    from matplotlib import pyplot as plt
    model = Model1().to(device)
    optimizer = torch.optim.Adam(model.parameters(),lr=1e-3)

    raw_data = dummy_dataset(512,384)
    data = dataset_mp(1,10,10,raw_data)
    data.start_batches()

    data.set_state("train seq")

    batch_size = 10 
    for epoch in range(10):
        print("### epoch:",epoch)

        train = 1
        if epoch>0 and epoch%2==0:
            train = 0

        if train == 1:
            print("#### TRAIN ###")
            data.set_state("train seq")

            to_gen = 30
        if train == 0:
            print("#### TEST ###")
            data.set_state("test seq")
            to_gen = len(data.dataset_raw.test_idx)//batch_size
       
        start = time.perf_counter()
        idx_all = []
        idx_all2 = []

        for it in tqdm(range(to_gen),ascii=True):
            batch,idx = data.get_batch_uniform(batch_size)
            inp = np.transpose(batch,axes=(0,-1,1,2))
            out = model(torch.tensor(inp).float().to(device))
            target = torch.rand(out.size()).to(device)
            loss = torch.mean(torch.pow(out-target,2))
            loss.backward()
            idx_ = list(np.mean(batch.reshape(batch.shape[0],-1),axis=-1))
            #print("idx_ after get_batch",idx_,idx)
            idx_diff = np.array(idx_)-np.array(idx)

            if np.sum(np.abs(idx_diff))>0:
                print("ACHTUNG!!!! DIFFERENT VALUES!!!!!!!!!!!!")
            idx_all+=list(idx_)
            idx_all2+=idx
        d = np.array(idx_all)
        d2 = np.array(idx_all2)
        if 1==2:
            d.sort()
            d2.sort()
            plt.figure()
            plt.plot(d,marker="*")
            plt.title("d epoch:"+str(epoch))
            plt.show()
            plt.figure()
            plt.plot(d2,marker="*")
            plt.title("d2 epoch:"+str(epoch))
            plt.show()

        print("epoch processing time:",time.perf_counter()-start)
        

if __name__ == '__main__':

    if 1==1:
        print("########### Learning, no batch generation #############")
        #learn_sequential(torch.device("cuda:0"),skip=True)
        print("########### Learning, 1-core batch generation #############")
        #learn_sequential(torch.device("cuda:0"),skip=False)
        print("####### Learning, distributed batch generation ################")
        learn_distributed(torch.device("cuda:0"))

